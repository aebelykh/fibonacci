
public class Main {

   private final static int MOD = (int) (1e9+7);

    private int fibanachi(int n){
        int a = 0;
        int b = 1;
        for (int i = 0; i < n ; i++) {
            int c = (a+b) % MOD;
            a = b;
            b = c;
        }
        return a;
    }
    private void run(int n){
        System.out.println(n + ": " + fibanachi(n));
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            Main main = new Main();
            main.run(i);
        }
    }
}
